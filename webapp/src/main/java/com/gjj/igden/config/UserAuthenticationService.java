package com.gjj.igden.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gjj.igden.model.Account;
import com.gjj.igden.service.AccountService;

@Service
public class UserAuthenticationService implements UserDetailsService {

	@Autowired
	private AccountService accountService;

	/**
	 * Load user by username
	 */
	public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException, DataAccessException {
		Account user = accountService.findByAccountName(accountName);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return user;
	}
}
