package com.gjj.igden.service;

import java.io.InputStream;
import java.util.List;

import com.gjj.igden.model.Account;

public interface AccountService {

	List<Account> getAccountList();

	boolean createAccount(Account account);

	boolean updateAccount(Account account);

	Account retrieveAccount(int accId);

	boolean delete(int id);

	boolean setImage(int accId, InputStream inputStream);

	byte[] getImage(int accId);

	Account findByAccountName(String accountName);

}
