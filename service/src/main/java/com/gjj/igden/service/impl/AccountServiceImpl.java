package com.gjj.igden.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.DataSet;
import com.gjj.igden.repository.AccountRepository;
import com.gjj.igden.repository.DataSetRepository;
import com.gjj.igden.service.AccountService;
import com.google.common.io.ByteStreams;

@Service
public class AccountServiceImpl implements AccountService {
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private DataSetRepository dataSetRepository;
	
	/**
	 * List all accounts
	 * @return
	 */
	@Override
	public List<Account> getAccountList() {
		return (List<Account>) accountRepository.findAll();
	}
	
	/**
	 * Save account
	 * @param account
	 * @return
	 */
	@Override
	public boolean createAccount(Account account) {
		Account isSavedAccount = accountRepository.save(account);
		if (isSavedAccount != null) {
			return true;
		} else {
			System.err.println(" something bad happen - account wasn't added ");
			return false;
		}
	}

	@Override
	public boolean updateAccount(Account account) {
		return accountRepository.save(account) != null;
	}
	
	@Override
	public Account retrieveAccount(int accId) {
		Account user = accountRepository.findByAccountId(accId);
		List<DataSet> dataSetList = dataSetRepository.findByAccountAccountId(accId);
		user.setDatasets(dataSetList);
		return user;
	}

	@Override
	public boolean delete(int id) {
		Account account = accountRepository.findByAccountId(id);
		try {
			accountRepository.delete(account);
			return true;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean setImage(int accId, InputStream inputStream) {
		Account account = accountRepository.findByAccountId(accId);
		byte[] bytes;
		try {
			bytes = ByteStreams.toByteArray(inputStream);
			account.setImage(bytes);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		return accountRepository.save(account) != null;
	}

	@Override
	public byte[] getImage(int accId) {
		Account account = accountRepository.findByAccountId(accId);
		return account.getImage();
	}

	@Override
	public Account findByAccountName(String accontName) {
		return accountRepository.findByAccountName(accontName);
	}
}
