package com.gjj.igden.service.impl;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gjj.igden.model.MarketData;
import com.gjj.igden.repository.MarketDataRepository;
import com.gjj.igden.service.MarketDataService;

@Service
public class MarketDataServiceImpl implements MarketDataService {

	@Autowired
	private MarketDataRepository marketDataRepository;
	
	@Override
	public boolean createBar(MarketData marketData) throws ServiceException {
		return marketDataRepository.save(marketData) != null;
	}

	@Override
	public boolean deleteBar(MarketData marketData) {
		try {
			marketDataRepository.delete(marketData);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
