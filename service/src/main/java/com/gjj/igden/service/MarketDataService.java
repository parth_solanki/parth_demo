package com.gjj.igden.service;

import org.hibernate.service.spi.ServiceException;

import com.gjj.igden.model.MarketData;

public interface MarketDataService {

	boolean createBar(MarketData bar) throws ServiceException;

	boolean deleteBar(MarketData marketData);

}
