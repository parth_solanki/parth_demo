package com.gjj.igden.repository;

import org.springframework.data.repository.CrudRepository;
import com.gjj.igden.model.MarketData;

public interface MarketDataRepository extends CrudRepository<MarketData, Integer> {

}
