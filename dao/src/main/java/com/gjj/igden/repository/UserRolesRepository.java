package com.gjj.igden.repository;

import org.springframework.data.repository.CrudRepository;

import com.gjj.igden.model.UserRoles;

public interface UserRolesRepository extends CrudRepository<UserRoles, Integer> {

}
