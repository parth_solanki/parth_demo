package com.gjj.igden.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gjj.igden.model.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

	Account findByAccountId(int accountId);

	Account findByAccountName(String accountName);

}
