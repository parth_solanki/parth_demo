package com.gjj.igden.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.gjj.igden.model.DataSet;

@Repository
@Transactional
public interface DataSetRepository extends CrudRepository<DataSet, Integer> {
	
	List<DataSet> findByAccountAccountId(Integer accountId);
}
