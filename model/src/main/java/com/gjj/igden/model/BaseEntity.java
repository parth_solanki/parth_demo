package com.gjj.igden.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="created_datetime")
	protected Date createdDatetime;
	
	@Column(name="modified_datetime")
	protected Date modifiedDatetime;
	
	@PrePersist
	public void prePersist(){
		this.createdDatetime	= new Date();
		this.modifiedDatetime	= this.createdDatetime;
	}
	
	@PreUpdate
	public void preUpdate(){
		this.modifiedDatetime = new Date();
	}

	protected Date getCreateDate() {
		return this.createdDatetime;
	}

	protected Date getModifyDate() {
		return this.modifiedDatetime;
	}

}
