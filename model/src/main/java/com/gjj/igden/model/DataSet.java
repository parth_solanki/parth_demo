package com.gjj.igden.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "data_set")
public class DataSet extends BaseEntity<Integer> implements Serializable {
	
	private static final long serialVersionUID = -1590086029171568556L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "data_set_id")
	private Integer dataSetId;
	
	@ManyToOne
	@JoinColumn(name="account_id",referencedColumnName="account_id")
	private Account account;
	
	@Column(name = "data_set_name")
	private String dataSetName;
	
	@Column(name = "data_set_description",length = 1024)
	private String dataSetDescription;
	
	@Column(name = "market_data_frequency")
	private Integer marketDataFrequency;
	
	@Column(name = "data_provider")
	private String dataProvider;

	public Integer getDataSetId() {
		return dataSetId;
	}

	public void setDataSetId(Integer dataSetId) {
		this.dataSetId = dataSetId;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getDataSetName() {
		return dataSetName;
	}

	public void setDataSetName(String dataSetName) {
		this.dataSetName = dataSetName;
	}

	public String getDataSetDescription() {
		return dataSetDescription;
	}

	public void setDataSetDescription(String dataSetDescription) {
		this.dataSetDescription = dataSetDescription;
	}

	public Integer getMarketDataFrequency() {
		return marketDataFrequency;
	}

	public void setMarketDataFrequency(Integer marketDataFrequency) {
		this.marketDataFrequency = marketDataFrequency;
	}

	public String getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(String dataProvider) {
		this.dataProvider = dataProvider;
	}
	
}
